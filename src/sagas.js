/**
 * @file init sagas
 */

import { all } from "redux-saga/effects";

// Place for sagas' app
// import { sagas as HomePage } from "modules/HomePage";
import { sagas as AuthPage } from "modules/AuthPage";
import { sagas as FriendsPage } from "modules/FriendsPage";
import { sagas as MessagesPage } from "modules/MessagesPage";
import { sagas as Wrapper } from "modules/Wrapper";

/*----Saga Root List-----------------*/
let sagasList = [AuthPage(), FriendsPage(), MessagesPage(), Wrapper()];

export default function* rootSaga(getState) {
	yield all(sagasList);
}
