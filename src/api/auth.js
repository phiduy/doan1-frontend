import config from "../config";
import { request } from "services/api";

const baseEndpoint = `${config.apiBaseURL}/users`;

export const login = (data) => {
	const endpoint = `${baseEndpoint}/login`;
	return request(endpoint, "POST", data);
};

export const register = (data) => {
	const endpoint = `${baseEndpoint}/register`;
	return request(endpoint, "POST", data);
};

export const getVerifyCode = (data) => {
	const endpoint = `${config.apiBaseURL}/verify`;
	return request(endpoint, "POST", data);
};

export const verifyRegister = (data) => {
	const endpoint = `${config.apiBaseURL}/verify/register`;
	return request(endpoint, "POST", data);
};

export const verifyForgotPassword = (data) => {
	const endpoint = `${config.apiBaseURL}/verify/forgotPassword`;
	return request(endpoint, "POST", data);
};

export const updateUserInfo = (data) => {
	const endpoint = `${baseEndpoint}`;
	return request(endpoint, "PUT", data);
};

export const getUserInfo = (data) => {
	const endpoint = `${baseEndpoint}`;
	return request(endpoint, "GET");
};
