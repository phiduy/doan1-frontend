import config from "../config";
import { request } from "services/api";

const baseEndpoint = `${config.apiBaseURL}/friends`;

export function searchUser(data) {
	const endpoint = `${config.apiBaseURL}/users/search?page=${data.page}&&limit=10`;
	return request(endpoint, "POST", data);
}

export function getAllFriends(data) {
	const endpoint = `${baseEndpoint}`;
	return request(endpoint, "GET");
}

export function getAllPending(data) {
	const endpoint = `${baseEndpoint}/pending`;
	return request(endpoint, "GET");
}

export function getAllRequests(data) {
	const endpoint = `${baseEndpoint}/request`;
	return request(endpoint, "GET");
}

export function postFriendRequest(data) {
	const endpoint = `${baseEndpoint}/request`;
	return request(endpoint, "POST", data);
}

export function postFriendAccept(data) {
	const endpoint = `${baseEndpoint}/accept`;
	return request(endpoint, "POST", data);
}

export function postFriendDeny(data) {
	const endpoint = `${baseEndpoint}/deny`;
	return request(endpoint, "POST", data);
}

export function postFriendCancelRequest(data) {
	const endpoint = `${baseEndpoint}/request`;
	return request(endpoint, "DELETE", data);
}
