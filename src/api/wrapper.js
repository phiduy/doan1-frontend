import config from "../config";
import { request } from "services/api";

const baseEndpoint = `${config.apiBaseURL}/rank`;

export function getSetting(data) {
	const endpoint = `${config.apiBaseURL}/config`;
	return request(endpoint, "GET");
}

export function getAllUniversities(data) {
	const endpoint = `${config.apiBaseURL}/university`;
	return request(endpoint, "GET");
}

export function getAllRanking(data) {
	const endpoint = `${baseEndpoint}/student?testTime${
		data.testTime > -1 ? `=${data.testTime}` : ""
	}&&page=${data.page}&&limit=${data.limit}`;
	return request(endpoint, "GET");
}

export function getAllRegisted(data) {
	const endpoint = `${baseEndpoint}/university/totalRegister?page=${data.page}&&limit=${data.limit}`;
	return request(endpoint, "GET");
}

export function getAllTested(data) {
	const endpoint = `${baseEndpoint}/university/totalTested?page=${data.page}&&limit=${data.limit}`;
	return request(endpoint, "GET");
}

export function getAllTestedByTime(data) {
	const endpoint = `${baseEndpoint}/university/totalByTestTime?${
		data.testTime > -1 ? `testTime=${data.testTime}&&` : ""
	}limit=${data.limit}&&page=${data.page}`;
	return request(endpoint, "GET");
}
