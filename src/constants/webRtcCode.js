const WEB_RTC_CODE = {
	RECEIVER_OFFLINE: "receiverOffline",
    RECEIVER_ON_CALL: "receiverOnCall",
    YOU_ARE_ON_CALL: "youOnCall",
    CALL_SUCCESS: "callSuccess",
    HAVING_A_CALL: "haveACall"
};

export default WEB_RTC_CODE