import React, { Component } from "react";
import { Switch, Route } from "react-router-dom";
import Wrapper from "modules/Wrapper/components/WrapperContainer";
import AuthPage from "modules/AuthPage/components/AuthPageContainer";
import FriendsPage from "modules/FriendsPage/components/FriendsPageContainer";
import MessagesPage from "modules/MessagesPage/components/MessagesPageContainer";
import "bootstrap/dist/css/bootstrap.min.css";

class App extends Component {
	render() {
		return (
			<React.Fragment>
				<Switch>
					<Wrapper>
						<Route path="/" exact component={AuthPage} />
						<Switch>
							<Route
								path="/friends"
								exact
								component={FriendsPage}
							/>
							<Route
								path="/messages"
								exact
								component={MessagesPage}
							/>
							{/* <Redirect from="*" to="/404" /> */}
						</Switch>
					</Wrapper>
				</Switch>
			</React.Fragment>
		);
	}
}

export default App;
