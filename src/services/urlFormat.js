import config from "../config";

const baseEndpoint = `${config.apiBaseURL}/`;


export const urlFormat = url => {
  return process.env.PUBLIC_URL + url;
};

export const urlExFormat = url => {
  return baseEndpoint + url;
};

export default {
  urlFormat,
  urlExFormat
};
