import { urlFormat } from "./urlFormat";

const audios = {
	audioCall: new Audio(urlFormat("/assets/audio/messenger_call.mp3")),
	audioNotify: new Audio(urlFormat("/assets/audio/messenger_notify.mp3")),
};

let currentlyPlaying = null;

const play = (name, loop = false) => {
	if (currentlyPlaying) {
		audios[currentlyPlaying].pause();
	}
	audios[name].loop = loop;
	audios[name].play();
	currentlyPlaying = name;
};

const pause = (name) => {
	currentlyPlaying = null;
	audios.currentTime = 0;
	audios[name].pause();
};

export default {
	pause,
	play,
};
