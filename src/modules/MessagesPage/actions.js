/**
 * @file All actions will be listed here
 */

import { createAction } from 'redux-actions';
import * as CONST from './constants';

export const getUserInfo = createAction(CONST.GET_USER_INFO);
export const getConnectRoom = createAction(CONST.GET_CONNECT_ROOM);

export const getAllSongs = createAction(CONST.GET_ALL_SONGS);
export const getAllSongsSuccess = createAction(CONST.GET_ALL_SONGS_SUCCESS);
export const getAllSongsFail = createAction(CONST.GET_ALL_SONGS_FAIL);

export const getAllRooms = createAction(CONST.GET_ALL_ROOMS);
export const getAllRoomsSuccess = createAction(CONST.GET_ALL_ROOMS_SUCCESS);
export const getAllRoomsFail = createAction(CONST.GET_ALL_ROOMS_FAIL);

export const getAllMessages = createAction(CONST.GET_ALL_MESSAGES);
export const getAllMessagesSuccess = createAction(CONST.GET_ALL_MESSAGES_SUCCESS);
export const getAllMessagesFail = createAction(CONST.GET_ALL_MESSAGES_FAIL);

export const getAllNotifications = createAction(CONST.GET_ALL_NOTIFICATIONS);
export const getAllNotificationsSuccess = createAction(CONST.GET_ALL_NOTIFICATIONS_SUCCESS);
export const getAllNotificationsFail = createAction(CONST.GET_ALL_NOTIFICATIONS_FAIL );

export const postSeenNotifications = createAction(CONST.POST_SEEN_NOTIFICATIONS);
export const postSeenNotificationsSuccess = createAction(CONST.POST_SEEN_NOTIFICATIONS_SUCCESS);
export const postSeenNotificationsFail = createAction(CONST.POST_SEEN_NOTIFICATIONS_FAIL);

export const postSeenMessage = createAction(CONST.POST_SEEN_MESSAGE);
export const postSeenMessageSuccess = createAction(CONST.POST_SEEN_MESSAGE_SUCCESS);
export const postSeenMessageFail = createAction(CONST.POST_SEEN_MESSAGE_FAIL);

export const uploadImage = createAction(CONST.UPLOAD_IMAGE);
export const uploadImageSuccess = createAction(CONST.UPLOAD_IMAGE_SUCCESS);
export const uploadImageFail = createAction(CONST.UPLOAD_IMAGE_FAIL);

export const uploadFile = createAction(CONST.UPLOAD_FILE);
export const uploadFileSuccess = createAction(CONST.UPLOAD_FILE_SUCCESS);
export const uploadFileFail = createAction(CONST.UPLOAD_FILE_FAIL);

export const updateMessages = createAction(CONST.UPDATE_MESSAGES);
export const updateRooms = createAction(CONST.UPDATE_ROOMS);
/*---------------------------------------------------------------------------------------------------------------------------------*/
export const toggleSideBar = createAction(CONST.TOGGLE_SIDE_BAR);
export const toggleModal = createAction(CONST.TOGGLE_MODAL);

export const handleReceiverAcceptCall = createAction(CONST.HANDLE_RECEIVER_ACCEPT_CALL)
export const handleCallerMakeCall = createAction(CONST.HANDLE_CALLER_MAKE_CALL);
export const handleCancelCall = createAction(CONST.HANDLE_CANCEL_CALL)

export const handleConnectWebRtcFail = createAction(CONST.HANDLE_CONNECT_WEB_RTC_FAIL);
export const handleConnectWebRtcSuccess = createAction(CONST.HANDLE_CONNECT_WEB_RTC_SUCCESS);

export const handleMakeMusicRequest = createAction(CONST.HANDLE_MAKE_MUSIC_REQUEST);

export const handleCurSong = createAction(CONST.HANDLE_CUR_SONG);
export const handleTyping = createAction(CONST.HANDLE_TYPING);
export const handleCurTab = createAction(CONST.HANDLE_CUR_TAB);
export const handleCurRoom = createAction(CONST.HANDLE_CUR_ROOM);
export const handleClear = createAction(CONST.HANDLE_CLEAR);