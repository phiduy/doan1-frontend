import { call, put } from "redux-saga/effects";
import * as actions from "../actions";
import { delay } from "redux-saga";
import { takeAction } from "services/forkActionSagas";
import * as wrapperActions from "../../Wrapper/actions";
import * as roomAPI from "api/room";

// Handle Music Request
export function* handleMusicRequest(action) {
	try {
		yield put(
			wrapperActions.handleProcessingMusic({
				code: "isRequester",
				response: {
					...action.payload,
				},
			})
		);
	} catch (err) {}
}

// Handle Accept Call
export function* handleAcceptCall(action) {
	try {
		yield put(
			wrapperActions.handleCalling({
				code: "onCall",
				response: {
					...action.payload,
				},
			})
		);
	} catch (err) {}
}

// Handle Upload Image
export function* handleUploadImage(action) {
	try {
		let res = yield call(roomAPI.uploadImage, action.payload);
		yield delay(500);
		switch (res.status) {
			case 200:
				yield put(actions.uploadImageSuccess(res.data.message));
				break;
			case 406:
				yield put(actions.uploadImageFail(res.data));
				break;
			default:
				break;
		}
	} catch (err) {
		yield put(actions.uploadImageFail(err));
	}
}

// Handle Upload File
export function* handleUploadFile(action) {
	try {
		let res = yield call(roomAPI.uploadFile, action.payload);
		yield delay(500);
		switch (res.status) {
			case 200:
				yield put(actions.uploadFileSuccess(res.data.message));
				break;
			case 406:
				yield put(actions.uploadFileFail(res.data));
				break;
			default:
				break;
		}
	} catch (err) {
		yield put(actions.uploadFileFail(err));
	}
}

// Handle Get All Notifications
export function* handleGetAllNotifications(action) {
	try {
		let res = yield call(roomAPI.getAllNotifications);
		if (res.status === 200) {
			yield put(actions.getAllNotificationsSuccess(res.data.data));
		} else {
			yield put(actions.getAllNotificationsFail(res.data));
		}
	} catch (err) {
		yield put(actions.getAllNotificationsFail(err));
	}
}
// Handle Seen Notifications
export function* handlePostSeenNotifications(action) {
	try {
		let res = yield call(roomAPI.postSeenNotifications);
		if (res.status === 200) {
			yield put(actions.postSeenMessageSuccess(res.data.data));
		} else {
			yield put(actions.postSeenMessageFail(res.data));
		}
	} catch (err) {
		yield put(actions.postSeenMessageFail(err));
	}
}

// Handle Seen Message
export function* handlePostSeenMessage(action) {
	try {
		let res = yield call(roomAPI.postSeenMessage, action.payload);
		yield delay(500);
		switch (res.status) {
			case 200:
				yield put(actions.postSeenMessageSuccess(res.data.message));
				break;
			case 406:
				yield put(actions.postSeenMessageFail(res.data));
				break;
			default:
				break;
		}
	} catch (err) {
		yield put(actions.postSeenMessageFail(err));
	}
}

// Handle Get All Messages
export function* handleGetAllMessages(action) {
	try {
		let res = yield call(roomAPI.getAllMessages, action.payload);
		yield delay(500);
		switch (res.status) {
			case 200:
				yield put(actions.getAllMessagesSuccess(res.data.data));
				break;
			case 406:
				yield put(actions.getAllMessagesFail(res.data));
				break;
			default:
				break;
		}
	} catch (err) {
		yield put(actions.getAllMessagesFail(err));
	}
}

// Handle Get All Rooms
export function* handleGetAllRooms(action) {
	try {
		let res = yield call(roomAPI.getAllRooms);
		yield delay(500);
		if (res.status === 200) {
			yield put(actions.getAllRoomsSuccess(res.data.data));
		} else {
			yield put(actions.getAllRoomsFail(res));
		}
	} catch (err) {
		yield put(actions.getAllRoomsFail(err));
	}
}

// Handle Get All Songs
export function* handleGetAllSongs(action) {
	try {
		let res = yield call(roomAPI.getAllSongs);
		yield delay(500);
		if (res.status === 200) {
			yield put(actions.getAllSongsSuccess(res.data.data));
		} else {
			yield put(actions.getAllSongsFail(res));
		}
	} catch (err) {
		yield put(actions.getAllSongsFail(err));
	}
}

/*-----------------------------------------------------------------*/
export function* getAllMessages() {
	yield takeAction(actions.getAllMessages, handleGetAllMessages);
}
export function* getAllNotifications() {
	yield takeAction(actions.getAllNotifications, handleGetAllNotifications);
}
export function* getAllRooms() {
	yield takeAction(actions.getAllRooms, handleGetAllRooms);
}
export function* getAllSongs() {
	yield takeAction(actions.getAllSongs, handleGetAllSongs);
}
export function* postSeenNotifications() {
	yield takeAction(
		actions.postSeenNotifications,
		handlePostSeenNotifications
	);
}
export function* postSeenMessage() {
	yield takeAction(actions.postSeenMessage, handlePostSeenMessage);
}
export function* uploadImage() {
	yield takeAction(actions.uploadImage, handleUploadImage);
}
export function* uploadFile() {
	yield takeAction(actions.uploadFile, handleUploadFile);
}
export function* acceptCall() {
	yield takeAction(actions.handleReceiverAcceptCall, handleAcceptCall);
}
export function* musicRequest() {
	yield takeAction(actions.handleMakeMusicRequest, handleMusicRequest);
}
/*-----------------------------------------------------------------*/
export default [
	getAllNotifications,
	getAllMessages,
	getAllRooms,
	getAllSongs,
	postSeenNotifications,
	postSeenMessage,
	uploadImage,
	uploadFile,
	acceptCall,
	musicRequest,
];
