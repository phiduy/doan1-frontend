import React from "react";
import ChatSideItem from "./ChatSideItem";
import ChatSideHeader from "./ChatSideHeader";
import styles from "./sidebar.module.css";
import _ from "lodash";

const chatSideBar = (props) => {
	const { listRooms, curRoom } = props;
	return (
		<React.Fragment>
			<div className="sidebar-group">
				<div id="chats" className="sidebar active">
					<ChatSideHeader {...props} />
					<div className={styles.sidebarBody}>
						<ul className="list-group list-group-flush">
							{!_.isEmpty(listRooms) ? (
								listRooms.map((room, index) => {
									return (
										<ChatSideItem
											actions={props.actions}
											roomInfo={room}
											curRoom={curRoom}
											key={`room__${index}`}
										/>
									);
								})
							) : (
								<li className="list-group-item open-chat">
									<p> You don't have any room chat</p>
								</li>
							)}
						</ul>
					</div>
				</div>
			</div>
		</React.Fragment>
	);
};

export default chatSideBar;
