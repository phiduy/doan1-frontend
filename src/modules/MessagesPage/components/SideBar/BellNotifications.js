import React from "react";
import styles from "./sidebar.module.css";
import moment from "moment";

const renderNotification = (listNotifications) =>
	listNotifications.map((notification) => {
		return (
			<div
				className={styles.bellNotificationItem}
				key={`notification__${notification._id}`}
			>
				{/* <div className={styles.bellNotificationItemProfileContent}>
					<img
						className={styles.bellNotificationItemProfile}
						src="https://c1.staticflickr.com/5/4007/4626436851_5629a97f30_b.jpg"
						alt="bell_avt"
					/>
				</div> */}
				<div className={styles.content}>
					<div className={styles.bellNotificationText}>
						{notification.notify}
					</div>
					<div
						className={`${styles.bellNotificationText} ${styles.bellNotificationTime}`}
					>
						{moment(notification.time).format("L HH:mm")}
					</div>
				</div>
			</div>
		);
	});

const BellNotifications = ({ listNotifications, actions }) => {
	const numberNotificationsNotSeen =
		listNotifications.filter((notification) => {
			return notification.seen === false;
		}).length > 0;

	return (
		<React.Fragment>
			{numberNotificationsNotSeen > 0 && (
				<div className={styles.bellNotificationNumber}>
					{numberNotificationsNotSeen}
				</div>
			)}
			<i className="fa fa-bell-o text-primary"></i>
			<div className={styles.bellNotificationListContainer}>
				<div className={styles.bellNotificationList}>
					<div className={styles.bellNotificationEmpty}>
						<i className="fa fa-child stick"></i>
						<div className="cent">
							Looks Like your all caught up!
						</div>
					</div>
					<div className={styles.bellNotificationListContent}>
						{renderNotification(listNotifications)}
					</div>
					{numberNotificationsNotSeen > 0 && (
						<div className={styles.bellNotificationBtnGroup}>
							<span
								className="btn btn-outline-light"
								onClick={() => actions.postSeenNotifications()}
							>
								Mark as read
							</span>
						</div>
					)}
				</div>
			</div>
		</React.Fragment>
	);
};

export default BellNotifications;
