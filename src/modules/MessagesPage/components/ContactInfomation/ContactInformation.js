import React from "react";
import { ReactSVG } from "react-svg";
import { urlFormat } from "services/urlFormat";
import classnames from "classnames";
import moment from "moment";
import "./ContactInformation.css";

const renderIconFile = (message) => {
	if (message.includes("xlsx")) {
		return <i className="fa fa-file-excel-o text-success mr-2"></i>;
	} else if (message.includes("txt")) {
		return <i className="fa fa-file-text-o text-warning mr-2"></i>;
	} else {
		return <i className="fa fa-file-text-o text-primary mr-2"></i>;
	}
};

const ContactInformation = (props) => {
	const { curRoom, isContactOpened, listImages, listFiles } = props;
	if (curRoom.hasOwnProperty('lastMessage')) {
		const { connectName, connectAvatar, lastMessage } = curRoom;
		return (
			<React.Fragment>
				<div className="sidebar-group">
					<div
						id="contact-information"
						className={classnames("sidebar", {
							"active overFHidden": isContactOpened,
						})}
					>
						<header>
							<span>Profile</span>
							<ul className="list-inline">
								<li className="list-inline-item">
									<span
										className="btn btn-outline-light text-danger"
										onClick={() =>
											props.actions.toggleSideBar({
												type: "contactInfo",
											})
										}
									>
										<ReactSVG
											src={urlFormat(
												"/assets/images/f-close.svg"
											)}
										/>
									</span>
								</li>
							</ul>
						</header>
						<div
							className="sidebar-body scroll_bar__thin"
							style={{
								overflow: "auto",
								outline: " currentcolor none medium",
							}}
							tabIndex={6}
						>
							<div className="pl-4 pr-4">
								<div className="text-center">
									<figure className="avatar avatar-xl mb-4">
										<img
											src={`https://${connectAvatar}`}
											className="rounded-circle"
											alt="avatar_profile_connector"
										/>
									</figure>
									<h5 className="mb-1">{connectName}</h5>
									<small className="text-muted font-italic">
										Last seen:{" "}
										{moment(lastMessage.time).format(
											"DD/MM/YYYY"
										)}
									</small>

									<ul
										className="nav nav-tabs justify-content-center mt-5"
										id="myTab"
										role="tablist"
									>
										<li className="nav-item">
											<a
												className="nav-link  active"
												id="media-tab"
												data-toggle="tab"
												href="#media"
												role="tab"
												aria-controls="media"
												aria-selected="false"
											>
												Media
											</a>
										</li>
										<li className="nav-item">
											<a
												className="nav-link"
												id="files-tab"
												data-toggle="tab"
												href="#files"
												role="tab"
												aria-controls="files"
												aria-selected="true"
											>
												Files
											</a>
										</li>
									</ul>
								</div>
								<div className="tab-content" id="myTabContent">
									<div
										className="tab-pane fade  show active"
										id="media"
										role="tabpanel"
										aria-labelledby="media-tab"
									>
										<div className="mediaContainer">
											{listImages.length < 1 ? (
												<h6 className="mb-3 d-flex align-items-center justify-content-between">
													<span>
														Don't have any image
													</span>
												</h6>
											) : (
												listImages.map((item) => {
													return (
														<div
															key={item._id}
															className="media-item"
														>
															<img
																src={
																	item.message
																}
																alt={item._id}
															/>
														</div>
													);
												})
											)}
										</div>
									</div>
									<div
										className="tab-pane fade"
										id="files"
										role="tabpanel"
										aria-labelledby="files-tab"
									>
										<h6 className="mb-3 d-flex align-items-center justify-content-between">
											<span>Recent Files</span>
											<span className="btn btn-link small">
												<ReactSVG
													src={urlFormat(
														"/assets/images/f-upload.svg"
													)}
												/>
												&nbsp; Upload
											</span>
										</h6>
										<div>
											<ul className="list-group list-group-flush">
												{listFiles.map((item) => {
													return (
														<li
															key={item._id}
															className="list-group-item pl-0 pr-0 d-flex align-items-center"
														>
															<a
																href={item.message}
															>
																{renderIconFile(
																	item.message
																)}
																{item.message.split(
																	"https://backend.pdchatapp.tk/uploads/"
																)}
															</a>
														</li>
													);
												})}
											</ul>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</React.Fragment>
		);
	}
	return null;
};

export default ContactInformation;
