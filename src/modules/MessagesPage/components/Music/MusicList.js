import React from "react";
import { ReactSVG } from "react-svg";
import { urlFormat } from "services/urlFormat";
import classnames from "classnames";
import MESSAGES from "../../../../constants/messages";
import { socket } from "services/api";

import "./MusicList.css";

const MusicList = (props) => {
	const { userId, curRoom, isMusicOpened, listSongs, curSong } = props;

	const handleMusicRequest = (song) => {
		props.actions.handleCurSong(song);
		props.actions.handleMakeMusicRequest({
			musicConnectId: curRoom.connectId,
			musicUrl: song.url,
			musicName: song.name,
		});
		socket.emit(MESSAGES.CLIENT_SEND_MUSIC_REQUEST, {
			userId,
			musicConnectId: curRoom.connectId,
			musicUrl: song.url,
			musicName: song.name,
		});
	};

	return (
		<React.Fragment>
			<div className="sidebar-group">
				<div
					id="music-list"
					className={classnames("sidebar", {
						"active overFHidden": isMusicOpened,
					})}
				>
					<header>
						<span>Music</span>
						<ul className="list-inline">
							<li className="list-inline-item">
								<span
									className="btn btn-outline-light text-danger"
									onClick={() =>
										props.actions.toggleSideBar({
											type: "musicList",
										})
									}
								>
									<ReactSVG
										src={urlFormat(
											"/assets/images/f-close.svg"
										)}
									/>
								</span>
							</li>
						</ul>
					</header>
					<div
						className="sidebar-body scroll_bar__thin"
						style={{
							overflow: "auto",
							outline: " currentcolor none medium",
						}}
						tabIndex={6}
					>
						<div className="text-center"></div>
						<div
							className="tab-pane fade  show active"
							id="musics"
							role="tabpanel"
							aria-labelledby="musics-tab"
						>
							<ul className="list-group list-group-flush">
								{listSongs.map((item) => {
									return (
										<li
											key={item._id}
											className={classnames(
												"list-group-item pl-4 pr-4 d-flex align-items-center song__container",
												{
													playingSong:
														curSong &&
														curSong._id ===
															item._id,
												}
											)}
											onClick={() =>
												handleMusicRequest(item)
											}
										>
											<div className="song__content">
												<div className="song__icon">
													<i
														className="fa fa-headphones"
														aria-hidden="true"
													></i>
												</div>
												<div className="song__desc">
													<h5>{item.singer}</h5>
													<span className="song_name">
														{item.name}
													</span>
												</div>
											</div>
										</li>
									);
								})}
							</ul>
						</div>
					</div>
				</div>
			</div>
		</React.Fragment>
	);
};

export default MusicList;
