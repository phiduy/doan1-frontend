import React from "react";
import "./musicplayer.css";

const MusicPlayer = (props) => {
	const [play, setPlay] = React.useState(false);

	return (
		<React.Fragment>
			<div className="player">
				<span id="arm" className={play ? "active" : ""}></span>
				<ul>
					<li className="artwork"></li>
					<li className="info">
						<h1 className="artist_name">loading</h1>
						<h4 className="album_name">loading</h4>
						<h2 className="song_name">loading</h2>
						<div className="button-items">
							<audio id="music" preload="auto" autoPlay></audio>
							<div id="slider">
								<div id="elapsed"></div>
								<div id="buffered"></div>
							</div>
							<p id="timer">0:00</p>
							<div className="controls">
								<span className="expend">
									<i
										className="fa fa-step-backward"
										aria-hidden="true"
									></i>
								</span>
								{play ? (
									<span className="text-center play" onClick={() => setPlay(!play)}>
										<i
											className="fa fa-pause-circle-o"
											aria-hidden="true"
										></i>
									</span>
								) : (
									<span className="text-center pause" onClick={() => setPlay(!play)}>
										<i
											className="fa fa-play-circle-o"
											aria-hidden="true"
										></i>
									</span>
								)}
								<span className="expend">
									<i
										className="fa fa-step-forward"
										aria-hidden="true"
									></i>
								</span>
								<div className="slider">
									<div className="volume"></div>
									<input
										type="range"
										id="volume"
										min="0"
										max="1"
										step="0.01"
										value="1"
									/>
								</div>
							</div>
						</div>
					</li>
				</ul>
			</div>
		</React.Fragment>
	);
};

export default MusicPlayer;
