import React, { Component } from "react";
import { ReactSVG } from "react-svg";
import FMenu from "../svg/f-menu.svg";
import FVideo from "../svg/f-video.svg";
import FPhone from "../svg/f-phone.svg";
import { socket } from "services/api";
import MESSAGES from "../../../../constants/messages";

class ChatHeader extends Component {
	handleUserMakeCall = () => {
		const { userId, curRoom } = this.props;
		socket.emit(MESSAGES.CLIENT_WEB_RTC_SEND_CALL_ID, {
			userId,
			callId: curRoom.connectId,
		});
		this.props.actions.handleCallerMakeCall({
			type: "videoCall",
		});
	};

	render() {
		const { curRoom } = this.props;
		return (
			<div className="chat-header">
				<div className="chat-header-user">
					<figure className="avatar">
						<img
							src={`https://${curRoom.connectAvatar}`}
							className="rounded-circle"
							alt="header_avt"
						/>
					</figure>
					<div>
						<h5>{curRoom.connectName}</h5>
					</div>
				</div>
				<div className="chat-header-action">
					<ul className="list-inline">
						<li className="list-inline-item d-xl-none d-inline">
							<span className="btn btn-outline-light mobile-navigation-button">
								<ReactSVG src={FMenu} />
							</span>
						</li>
						<li className="list-inline-item">
							<span className="btn btn-outline-light text-success">
								<ReactSVG src={FPhone} />
							</span>
						</li>
						<li className="list-inline-item">
							<span
								className="btn btn-outline-light text-warning"
								onClick={this.handleUserMakeCall}
							>
								<ReactSVG src={FVideo} />
							</span>
						</li>
						<li className="list-inline-item">
							<span
								className="btn btn-outline-light text-primary"
                                onClick={ () =>
                                    this.props.actions.toggleSideBar({
										type: "musicList",
									})
                                }
							>
								<i
									className="fa fa-headphones"
									aria-hidden="true"
								></i>
							</span>
						</li>
						<li className="list-inline-item">
							<span
								className="btn btn-outline-light text-secondary"
								onClick={() =>
									this.props.actions.toggleSideBar({
										type: "contactInfo",
									})
								}
							>
								<i
									className="fa fa-info"
									aria-hidden="true"
								></i>
							</span>
						</li>
					</ul>
				</div>
			</div>
		);
	}
}

export default ChatHeader;
