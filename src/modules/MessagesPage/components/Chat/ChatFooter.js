import React, { useRef, useState, useEffect } from "react";
import { urlFormat } from "services/urlFormat";
import { ReactSVG } from "react-svg";
import FSmile from "../svg/f-smile.svg";
import FPaperClip from "../svg/f-paper-clip.svg";
import FSend from "../svg/f-send.svg";
import { socket } from "services/api";
import MESSAGES from "../../../../constants/messages";
import { Picker } from "emoji-mart";
import classnames from "classnames";
import * as styles from "./chat.module.css";

const usePrevious = (value) => {
	const ref = useRef();
	useEffect(() => {
		ref.current = value;
	});
	return ref.current;
};

const previewDefault = {
	url: null,
	file: null,
	type: "image",
};

const imagePreview = (previewFile, setPreviewFile) => (
	<div className={styles.chat_footer__preview}>
		<div className={styles.chat_footer__image}>
			<div
				className={styles.delete__file}
				onClick={() => setPreviewFile(previewDefault)}
			>
				<i className="fa fa-times"></i>
			</div>
			<img src={previewFile.url} alt="image__preview" />
		</div>
	</div>
);

const filePreview = (previewFile, setPreviewFile) => (
	<div className={styles.chat_footer__preview}>
		<div className={styles.chat_footer__file}>
			<div
				className={styles.delete__file}
				onClick={() => setPreviewFile(previewDefault)}
			>
				<i className="fa fa-times"></i>
			</div>
			<div className={styles.chat_footer__file_content}>
				<div className={styles.chat_footer__file_icon}>
					<div className={styles.chat_footer__file_i}>
						<i className="fa fa-file" />
					</div>
				</div>
				<div className={styles.chat_footer__file_desc}>
					<p className={styles.chat_file_type}>
						{previewFile.tailOfFile}
					</p>
					<p className={styles.chat_file_name}>
						{previewFile.file.name}
					</p>
				</div>
			</div>
		</div>
	</div>
);

const classifyPreview = (previewFile, setPreviewFile) => {
	if (previewFile.type === "file") {
		return filePreview(previewFile, setPreviewFile);
	}
	return imagePreview(previewFile, setPreviewFile);
};

const Chatting = (props) => {
	const [message, setMessage] = useState("");
	const [isOpenedIcon, setOpenIcon] = useState(false);
	const [previewFile, setPreviewFile] = useState(previewDefault);
	const { userId, curRoom, urlImage, isUploadSuccess, isUploading } = props;
	const prevIsUpload = usePrevious(isUploading);
	const prevIsUploadSuccess = usePrevious(isUploadSuccess);

	useEffect(() => {
		const { roomId, connectId } = curRoom;
		let data = {
			userId,
			roomId: roomId,
			sendingId: connectId,
			type: previewFile.type,
			message: `https://${urlImage}`,
		};
		// if (prevIsUpload !== isUploading && isUploading) {
		// 	setPreviewFile(previewDefault);
		// }

		if (prevIsUploadSuccess !== isUploadSuccess && isUploadSuccess) {
			socket.emit(MESSAGES.CLIENT_SEND_MESSAGE, data);
			props.actions.updateMessages(data);
			props.actions.updateRooms({
				roomId: curRoom.roomId,
				lastMessage: {
					type: previewFile.type,
					message: data.message,
					time: Date.now(),
				},
			});
			setPreviewFile(previewDefault);
		}
	}, [
		props.actions,
		curRoom,
		urlImage,
		userId,
		isUploadSuccess,
		prevIsUploadSuccess,
		isUploading,
		prevIsUpload,
		previewFile,
	]);

	const handleSubmit = (e) => {
		if (previewFile.file !== null) {
			if (previewFile.type !== "image") {
				props.actions.uploadFile(previewFile.file);
			} else {
				props.actions.uploadImage(previewFile.file);
			}
		}
		if (message !== "") {
			let data = {
				userId,
				roomId: curRoom.roomId,
				sendingId: curRoom.connectId,
				type: "text",
				message: message,
			};
			socket.emit(MESSAGES.CLIENT_SEND_MESSAGE, data);
			props.actions.updateMessages(data);
			props.actions.updateRooms({
				roomId: curRoom.roomId,
				lastMessage: {
					type: "text",
					message: message,
					time: Date.now(),
				},
			});
			setMessage("");
		}
		e.preventDefault();
	};

	const handleUploadFile = () => {
		if (previewFile.file === null) {
			const input = document.createElement("input");
			input.setAttribute("type", "file");
			input.setAttribute(
				"accept",
				"application/docx, application/xlsx application/vnd.ms-excel, application/vnd.ms-powerpoint, text/plain, application/pdf, image/*"
			);
			input.click();
			input.onchange = () => {
				const file = input.files[0];
				let tailOfFile = file.name.split(".");
				let previewFile = {
					type: "file",
					tailOfFile: tailOfFile[tailOfFile.length - 1],
					file,
				};
				if (file.type.includes("image/")) {
					previewFile = {
						file,
						url: URL.createObjectURL(file),
						type: "image",
					};
				}
				setPreviewFile(previewFile);
			};
		}
	};

	const handleTyping = (e) => {
		let data = {
			userId,
			roomId: curRoom.roomId,
		};
		socket.emit(MESSAGES.CLIENT_SEND_TYPING, data);
		setMessage(e.target.value);
	};

	return (
		<React.Fragment>
			<div className="chat-footer">
				{previewFile.url !== null &&
					classifyPreview(previewFile, setPreviewFile)}
				<form onSubmit={handleSubmit}>
					<div>
						<div style={{ position: "relative" }}>
							<button
								className="btn btn-light mr-3"
								type="button"
								onClick={() => setOpenIcon(!isOpenedIcon)}
							>
								<ReactSVG src={FSmile} />
							</button>
							<div
								className={classnames("emoji-container ", {
									show: isOpenedIcon,
								})}
							>
								<Picker
									onSelect={(emoji) => {
										setOpenIcon(false);
										setMessage(`${message}${emoji.native}`);
									}}
								/>
							</div>
						</div>
					</div>
					<textarea
						type="text"
						className="form-control"
						placeholder="Write a message."
						value={message}
						onFocus={() => setOpenIcon(false)}
						onChange={(e) => handleTyping(e)}
						onKeyPress={(e) => {
							if (e.key === "Enter") {
								handleSubmit(e);
							}
						}}
					/>
					<div className="form-buttons">
						<span
							className="btn btn-light"
							data-toggle="tooltip"
							type="button"
							data-original-title="Add files"
							onClick={handleUploadFile}
						>
							<ReactSVG src={FPaperClip} />
						</span>
						<button
							className="btn btn-light d-sm-none d-block"
							data-toggle="tooltip"
							type="button"
							data-original-title="Send a voice record"
						>
							<img
								src={urlFormat("/assets/images/f-mic.svg")}
								alt="send"
								width="16px"
								height="16px"
							/>
						</button>
						<button className="btn btn-primary" type="submit">
							<ReactSVG src={FSend} />
						</button>
					</div>
				</form>
			</div>
		</React.Fragment>
	);
};

export default Chatting;
