import React from "react";
import ChatFooter from "./ChatFooter";
import ChatHeader from "./ChatHeader";
import ChatMessage from "./ChatMessage";
import LoadingScreen from "../../../common/LoadingScreen";
import "./chat.module.css";

const ChatMain = (props) => {
	const { isLoadingMessages } = props;
	return (
		<React.Fragment>
			<div className="chat">
				<ChatHeader {...props} />
				{isLoadingMessages ? (
					<div style={{ width: "100%", height: "100%" }}>
						<LoadingScreen />
					</div>
				) : (
					<>
						<ChatMessage {...props} />
						<ChatFooter {...props} />
					</>
				)}
			</div>
		</React.Fragment>
	);
};

export default ChatMain;
