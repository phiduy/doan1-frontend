import React from "react";
import classnames from "classnames";
import moment from "moment";
import * as styles from "./chat.module.css";

const renderMessageWithType = (item, loadingImage) => {
	let node = React.node;
	switch (item.type) {
		case "text":
			node = <div className="message-content">{item.message}</div>;
			break;
		case "image":
			node = (
				<figure className="message-image">
					<div className={styles.message_image_preview}>
						{loadingImage ? (
							<div
								className="spinner-border text-primary"
								role="status"
							>
								<span className="sr-only">Loading...</span>
							</div>
						) : (
							<img
								src={item.message}
								className="img-fluid"
								alt="upload__image"
							/>
						)}
					</div>
				</figure>
			);
			break;
		case "file":
			node = (
				<div className="message-content message-file">
					<div className="file-icon">
						<i className="fa fa-file"></i>
					</div>
					<div>
						<div>
							{/* {item.message.split(`${config.apiURL}/uploads/`)} */}
							{item.message.split(
								"https://backend.pdchatapp.tk/uploads/"
							)}
							{/* <i className="text-muted small">(50KB)</i> */}
						</div>
						<ul className="list-inline">
							<li className="list-inline-item mb-0">
								<a
									href={item.message}
									target="_blank"
									rel="noopener noreferrer"
								>
									Download
								</a>
							</li>
						</ul>
					</div>
				</div>
			);
			break;
		default:
			break;
	}
	return node;
};

const chatMessageItem = ({
	item,
	userId,
	userInfo,
	curRoom,
	loadingImage = false,
}) => {
	return (
		<div
			className={classnames("message-item", {
				"outgoing-message right-message": item.userId === userId,
			})}
		>
			<div className="message-avatar">
				<figure className="avatar">
					<img
						src={`https://${
							item.userId === userId
								? userInfo.userAvatar
								: curRoom.connectAvatar
						}`}
						className="rounded-circle"
						alt={
							item.userId === userId
								? "avt_user"
								: "avt_connector"
						}
					/>
				</figure>
				<div>
					<h5>
						{item.userId === userId ? "You" : curRoom.connectName}
					</h5>
					<div className="time">
						{moment(item.time).format("HH:mm A")}&nbsp;
						{item.userId === userId && (
							<i className="fa fa-check text-info"></i>
						)}
					</div>
				</div>
			</div>
			{renderMessageWithType(item, loadingImage)}
			{item.userId !== userId && (
				<small>
					<i>{item.seen ? "seen" : ""}</i>
				</small>
			)}
		</div>
	);
};

export default chatMessageItem;
