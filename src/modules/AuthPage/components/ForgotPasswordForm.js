import React from "react";
import { FormGroup, Input, FormFeedback } from "reactstrap";
import { useForm } from "react-hook-form";
import { urlFormat } from "services/urlFormat";

export default function App(props) {
	const { register, errors, handleSubmit } = useForm();
	const { isVerifyCode, isProcessing } = props;
	const onSubmit = (data, e) => {
		props.actions.toggleLoading(true);
		if (isVerifyCode) {
			props.actions.updatePassword(data);
		} else {
			props.actions.forgotPassword({
				userEmail: data.userEmail,
				verifyType: "ForgotPassword",
			});
		}
	};

	return (
		<React.Fragment>
			<div className="form-wrapper">
				<div className="logo">
					<img
						src={urlFormat("/assets/images/logo.svg")}
						alt="logo"
					/>
				</div>

				<h5>Forgot password</h5>

				<form onSubmit={handleSubmit(onSubmit)}>
					{isVerifyCode ? (
						<React.Fragment>
							<FormGroup>
								<Input
									type="text"
									name="verifyCode"
									placeholder="Verify code"
									innerRef={register({
										required: "This field is required",
									})}
									invalid={errors.verifyCode ? true : false}
									disabled={isProcessing}
								/>
								<FormFeedback>
									{errors.verifyCode &&
										errors.verifyCode.message}
								</FormFeedback>
							</FormGroup>
							<FormGroup>
								<Input
									type="password"
									name="password"
									placeholder="Password"
									innerRef={register({
										required: "This field is required",
									})}
									invalid={errors.password ? true : false}
									disabled={isProcessing}
								/>
								<FormFeedback>
									{errors.password && errors.password.message}
								</FormFeedback>
							</FormGroup>
						</React.Fragment>
					) : (
						<FormGroup>
							<Input
								type="text"
								name="userEmail"
								placeholder="E-mail"
								innerRef={register({
									required: "This field is required",
								})}
								invalid={errors.userEmail ? true : false}
								disabled={isProcessing}
							/>
							<FormFeedback>
								{errors.userEmail && errors.userEmail.message}
							</FormFeedback>
						</FormGroup>
					)}

					<button type="submit" className="btn btn-primary btn-block">
						{isProcessing ? (
							<i className="fa fa-spinner fa-spin"></i>
						) : (
							"Send"
						)}
					</button>
					<hr />
					<span
						className="btn btn-outline-light btn-sm"
						onClick={() => props.actions.toggleForm("forgotForm")}
					>
						Back
					</span>
				</form>
			</div>
		</React.Fragment>
	);
}
