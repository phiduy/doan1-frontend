import React, { Component } from "react";
import SignInFrom from "./SignInForm";
import SignUpFrom from "./SignUpForm";
import VerifyForm from "./VerifyForm";
import ForgotPasswordForm from "./ForgotPasswordForm";
import NotificationSystem from "react-notification-system";

class AuthPage extends Component {
	notificationSystem = React.createRef();
	componentDidUpdate(prevProps) {
		const { isNotifying, notificationSetup } = this.props;
		if (prevProps.isNotifying !== isNotifying && isNotifying) {
			const notification = this.notificationSystem.current;
            notification.addNotification(notificationSetup);
            this.props.actions.handleClear("clearNotify")
		}
	}
	componentWillUnmount() {
		this.notificationSystem.current.clearNotifications();
	}
	render() {
		const {
			toggleAuthForm,
			toggleVerifyForm,
			toggleForgotForm,
		} = this.props;
		return (
			<React.Fragment>
				<div className="form-membership">
					{toggleForgotForm ? (
						<ForgotPasswordForm {...this.props} />
					) : toggleVerifyForm ? (
						<VerifyForm {...this.props} />
					) : toggleAuthForm ? (
						<SignUpFrom {...this.props} />
					) : (
						<SignInFrom {...this.props} />
					)}
				</div>
				<NotificationSystem ref={this.notificationSystem} />
			</React.Fragment>
		);
	}
}

export default AuthPage;
