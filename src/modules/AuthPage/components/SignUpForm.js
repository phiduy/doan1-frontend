import React from "react";
import { FormGroup, Input, FormFeedback } from "reactstrap";
import { useForm } from "react-hook-form";
import { urlFormat } from "services/urlFormat";
import Select from "react-select";
const Genders = [
	{ label: "Male", value: "male" },
	{ label: "Female", value: "female" },
];
export default function App(props) {
	const { register, errors, handleSubmit } = useForm();
	const { gender, isProcessing } = props;
	const onSubmit = (data, e) => {
		props.actions.toggleLoading(true);
		props.actions.register({
			...data,
			userGender: gender.value,
		});
	};

	return (
		<React.Fragment>
			<div className="form-wrapper">
				<div className="logo">
					<img
						src={urlFormat("/assets/images/logo.svg")}
						alt="logo"
					/>
				</div>

				<h5>Create account</h5>

				<form onSubmit={handleSubmit(onSubmit)}>
					<FormGroup>
						<Input
							type="text"
							name="userName"
							placeholder="Full name"
							innerRef={register({
								required: "This field is required",
								maxLength: {
									value: 50,
									message:
										"Please enter no more than 50 characters",
								},
							})}
							autoFocus
							invalid={errors.userName ? true : false}
							disabled={isProcessing}
						/>
						<FormFeedback>
							{errors.userName && errors.userName.message}
						</FormFeedback>
					</FormGroup>
					<FormGroup>
						<Input
							type="text"
							name="userEmail"
							placeholder="E-mail"
							innerRef={register({
								required: "This field is required",
								pattern: {
									value: /^([a-zA-Z0-9_\-\.]+)@([a-zA-Z0-9_\-\.]+)\.([a-zA-Z]{2,5})$/,
									message: "Invalid email",
								},
							})}
							invalid={errors.userEmail ? true : false}
							disabled={isProcessing}
						/>
						<FormFeedback>
							{errors.userEmail && errors.userEmail.message}
						</FormFeedback>
					</FormGroup>
					<FormGroup>
						<Select
							placeholder="Gender"
                            options={Genders}
                            defaultValue={gender.value}
							defaultInputValue={gender.label}
							onChange={(e) =>
								props.actions.handleSelectChange(e)
                            }
                            isSearchable={false}
							disabled={isProcessing}
						/>
						<FormFeedback>
							{errors.userGender && errors.userGender.message}
						</FormFeedback>
					</FormGroup>
					<FormGroup>
						<Input
							type="password"
							name="userPassword"
							placeholder="Password"
							innerRef={register({
								required: "This field is required",
								pattern: {
									value: /^(?=.*?[A-Z])(?=.*?[a-z])(?=.*?[0-9])(?=.*?[^\w\s]).{6,}$/,
									message:
										"Password must contain at least 6 characters, 1 numeric character, 1 lowercase letter, 1 uppercase letter and 1 special character",
								},
							})}
							disabled={isProcessing}
							invalid={errors.userPassword ? true : false}
						/>
						<FormFeedback>
							{errors.userPassword && errors.userPassword.message}
						</FormFeedback>
					</FormGroup>

					<button type="submit" className="btn btn-primary btn-block">
						{isProcessing ? (
							<i className="fa fa-spinner fa-spin"></i>
						) : (
							"Sign Up!"
						)}
					</button>
					<hr />
					<p className="text-muted">Already have an account?</p>
					<button
						onClick={() => props.actions.toggleForm("authForm")}
						className="btn btn-outline-light btn-sm"
					>
						Sign In!
					</button>
				</form>
			</div>
		</React.Fragment>
	);
}
