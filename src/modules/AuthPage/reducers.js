/**
 * @file reducer
 */

// Using to control stage

import freeze from "deep-freeze";
import { handleActions } from "redux-actions";
import * as actions from "./actions";

export const name = "AuthPage";

const initialState = freeze({
	toggleAuthForm: false,
	toggleVerifyForm: false,
	toggleForgotForm: false,
	isProcessing: false,
	isVerifyCode: false,
	isNotifying: false,
	notificationSetup: {
		title: "Thông báo",
		autoDismiss: 3,
		position: "tr",
		uid: 1,
	},
	gender: { label: "Male", value: "male" },
});

export default handleActions(
	{
		/*--
        ----  Toggle Loading
        */
		[actions.toggleLoading]: (state, action) => {
			return freeze({
				...state,
				isProcessing: action.payload,
			});
		},
		/*--
        ----  Toggle Form
        */
		[actions.toggleForm]: (state, action) => {
			if (action.payload === "authForm") {
				return freeze({
					...state,
					toggleAuthForm: !state.toggleAuthForm,
				});
			}
			if (action.payload === "verifyForm") {
				return freeze({
					...state,
					toggleVerifyForm: !state.toggleVerifyForm,
				});
			}
			if (action.payload === "forgotForm") {
				return freeze({
					...state,
					toggleForgotForm: !state.toggleForgotForm,
				});
			}
			return freeze({
				...state,
			});
		},
		/*--
        ----  Toggle Loading
        */
		[actions.notify]: (state, action) => {
			return freeze({
				...state,
				notificationSetup: { ...state.notificationSetup, ...action.payload },
				isNotifying: true,
			});
		},
		/*--
        ---- Handle Clear
        */
		[actions.handleClear]: (state, action) => {
			if (action.payload.type === "clearNotify") {
				return freeze({
					...state,
					isNotifying: false,
				});
			}
			return freeze({
				...initialState,
			});
		},
		/*--
        ---- Handle Select Change
        */
		[actions.handleSelectChange]: (state, action) => {
			return freeze({
				...state,
				gender: action.payload,
			});
		},
		/*--
        ---- Register
        */
		[actions.registerSuccess]: (state, action) => {
			return freeze({
				...state,

				toggleVerifyForm: true,
			});
		},
		[actions.registerFail]: (state, action) => {
			return freeze({
				...state,

				toggleVerifyForm: true,
			});
		},
		/*--
        ---- Forgot password
        */
		[actions.forgotPasswordSuccess]: (state, action) => {
			return freeze({
				...state,

				isVerifyCode: true,
			});
		},
		[actions.forgotPasswordFail]: (state, action) => {
			return freeze({
				...state,

				isVerifyCode: true,
			});
		},
		/*--
        ---- Update password
        */
		[actions.updatePasswordSuccess]: (state, action) => {
			return freeze({
				...state,

				isVerifyCode: false,
				toggleForgotForm: false,
			});
		},
		/*--
        ---- Verify Register
        */
		[actions.verifyRegisterSuccess]: (state, action) => {
			return freeze({
				...state,

				toggleAuthForm: false,
				toggleVerifyForm: false,
			});
		},
		[actions.verifyRegisterFail]: (state, action) => {
			return freeze({
				...state,

				toggleAuthForm: false,
				toggleVerifyForm: false,
			});
		},
	},
	initialState
);
