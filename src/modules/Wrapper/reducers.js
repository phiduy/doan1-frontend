/**
 * @file reducer
 */

// Using to control stage

import freeze from "deep-freeze";
import { handleActions } from "redux-actions";
import * as actions from "./actions";

export const name = "Wrapper";

const initialState = freeze({
	toggleModalUpdate: false,
	userInfo: {},
	updateUserInfo: {},
	isCaller: false,
	isReceiver: false,
	isOnCall: false,
	isFullScreen: false,
	isProcessing: false,
	isUploading: false,
	isUpdateProfileSuccess: false,
	isUploadAvtSuccess: false,

	isRequester: false,
	musicConnectId: null,
	musicConnectSocketId: null,
	musicUrl: null,
	musicName: null,
	isOnPlay: false,
	isCancelledRequestMusic: false,
	isOnUseMusic: false,

	gender: {},
	genderOptions: [
		{ label: "Male", value: "male" },
		{ label: "Female", value: "female" },
		{ label: "Others", value: "female" },
	],
	urlImage: null,
	answerWebRtcId: "",
	answerSocketId: "",
});

export default handleActions(
	{
		/*--
        ---- Handle Select Change
        */
		[actions.handleSelectChange]: (state, action) => {
			return freeze({
				...state,
				gender: action.payload,
			});
		},
		/*---
        ----- Toggle Modal Update
        */
		[actions.toggleModalUpdate]: (state, action) => {
			if (!state.toggleModalUpdate === false) {
				return freeze({
					...state,
					urlImage: null,
					toggleModalUpdate: !state.toggleModalUpdate,
					isUpdateProfileSuccess: false,
					isUploadAvtSuccess: false,
				});
			}
			return freeze({
				...state,
				toggleModalUpdate: !state.toggleModalUpdate,
			});
		},
		/*---
        ----- Handle Calling
        */
		[actions.handleFullScreen]: (state, action) => {
			return freeze({
				...state,
				isFullScreen: !state.isFullScreen,
			});
		},
		/*---
        ----- Handle Calling
        */
		[actions.handleCalling]: (state, action) => {
			let { code, response } = action.payload;
			switch (code) {
				case "callInfo":
					return freeze({
						...state,
						isCaller: true,
						isOnCall: true,
						answerWebRtcId: response.answerWebRtcId,
						answerSocketId: response.answerSocketId,
					});
				case "onCall":
					return freeze({
						...state,
						isOnCall: true,
						answerWebRtcId: action.payload.response.answerId,
						answerSocketId: action.payload.response.answerSocketId,
					});
				case "close":
					return freeze({
						...state,
						isOnCall: false,
						isFullScreen: false,
					});
				case "isReceiver":
					return freeze({
						...state,
						isReceiver: true,
						isOnCall: false,
						answerWebRtcId: response.callerId,
						answerSocketId: response.callerSocketId,
					});
				case "isCaller":
					return freeze({
						...state,
						isReceiver: false,
						isOnCall: false,
						answerWebRtcId: response.callId,
					});
				default:
					return freeze({
						...state,
					});
			}
		},
		/*---
        ----- Handle Calling
        */
		[actions.handleProcessingMusic]: (state, action) => {
			let code = action.payload.code;

			switch (code) {
				case "isRequester":
					return freeze({
						...state,
						isRequester: true,
						isOnUseMusic: true,
						isCancelledRequestMusic: true,
						musicConnectId: action.payload.response.musicConnectId,
						musicUrl: action.payload.response.musicUrl,
						musicName: action.payload.response.musicName,
					});
				case "stopMusic":
					return freeze({
						...state,
						isOnPlay: false,
						isRequester: false,
						isOnUseMusic: false,
						musicConnectId: null,
						musicUrl: null,
						musicName: null,
						isCancelledRequestMusic: true,
						musicConnectSocketId: null,
					});
				case "stopSendRequest":
					return freeze({
						...state,
						musicConnectId: null,
						musicUrl: null,
						musicName: null,
						musicConnectSocketId: null,
						isCancelledRequestMusic: true,
					});
				case "playMusic":
					return freeze({
						...state,
						isOnPlay: true,
						isCancelledRequestMusic: false,
					});
				case "isListener":
					return freeze({
						...state,
						isRequester: false,
						isOnUseMusic: true,
						isCancelledRequestMusic: false,
						musicName: action.payload.response.musicName,
						musicConnectId: action.payload.response.musicConnectId,
						musicUrl: action.payload.response.musicUrl,
						musicConnectSocketId:
							action.payload.response.musicConnectSocketId,
					});
				default:
					return freeze({
						...state,
					});
			}
		},
		/*---
        ----- Get User Info
        */
		[actions.getUserInfoSuccess]: (state, action) => {
			let gender = state.genderOptions.find(
				(gender) => gender.value === action.payload.userGender
			);
			return freeze({
				...state,
				userInfo: action.payload,
				gender,
			});
		},
		/*---
        ----- Update User Info
        */
		[actions.updateUserInfo]: (state, action) => {
			return freeze({
				...state,
				isUpdateProfileSuccess: false,
				isProcessing: true,
			});
		},
		[actions.updateUserInfoSuccess]: (state, action) => {
			return freeze({
				...state,
				isProcessing: false,
				isUpdateProfileSuccess: true,
				userInfo: action.payload,
			});
		},
		[actions.updateUserInfoFail]: (state, action) => {
			return freeze({
				...state,
				isProcessing: false,
				isUpdateProfileSuccess: false,
			});
		},
		/*---
        ----- Upload Avt
        */
		[actions.uploadAvt]: (state, action) => {
			return freeze({
				...state,
				isProcessing: true,
				updateUserInfo: action.payload.updateUserInfo,
				isUploadAvtSuccess: false,
			});
		},
		[actions.uploadAvtSuccess]: (state, action) => {
			return freeze({
				...state,
				isUploadAvtSuccess: true,
				urlImage: action.payload.url,
			});
		},
		[actions.uploadAvtFail]: (state, action) => {
			return freeze({
				...state,
				isUploadAvtSuccess: false,
			});
		},

		/*---
        ----- Handle Clear
        */
		[actions.handleClear]: (state, action) => {
			return freeze({
				...initialState,
			});
		},
	},
	initialState
);
