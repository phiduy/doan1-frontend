/**
 * @file All actions will be listed here
 */

import { createAction } from "redux-actions";
import * as CONST from "./constants";

export const logOut = createAction(CONST.LOG_OUT);

export const checkLogin = createAction(CONST.CHECK_LOGIN);
export const checkLoginSuccess = createAction(CONST.CHECK_LOGIN_SUCCESS);
export const checkLoginFail = createAction(CONST.CHECK_LOGIN_FAIL);
/*---------------------------------------------------------------------------------------------------------------------------------*/

export const getUserInfo = createAction(CONST.GET_USER_INFO);
export const getUserInfoSuccess = createAction(CONST.GET_USER_INFO_SUCCESS);
export const getUserInfoFail = createAction(CONST.GET_USER_INFO_FAIL);

export const uploadAvt = createAction(CONST.UPLOAD_AVT);
export const uploadAvtSuccess = createAction(CONST.UPLOAD_AVT_SUCCESS);
export const uploadAvtFail = createAction(CONST.UPLOAD_AVT_FAIL);

export const updateUserInfo = createAction(CONST.UPDATE_USER_INFO);
export const updateUserInfoSuccess = createAction(CONST.UPDATE_USER_INFO_SUCCESS);
export const updateUserInfoFail = createAction(CONST.UPDATE_USER_INFO_FAIL);
/*---------------------------------------------------------------------*/
export const handleCalling = createAction(CONST.HANDLE_CALLING);

export const handleProcessingMusic = createAction(CONST.HANDLE_PROCESSING_MUSIC);

export const handleSelectChange = createAction(CONST.HANDLE_SELECT_CHANGE);
export const handleFullScreen = createAction(CONST.HANDLE_FULL_SCREEN);
export const handleClear = createAction(CONST.HANDLE_CLEAR);

export const toggleModalUpdate = createAction(CONST.TOGGLE_MODAL_UPDATE);
