import React, { useState, useEffect, useRef } from "react";
import {
	FormGroup,
	InputGroup,
	InputGroupAddon,
	InputGroupText,
	Input,
	FormFeedback,
	ModalBody,
	ModalFooter,
	Alert,
} from "reactstrap";
import { useForm } from "react-hook-form";
import Select from "react-select";
import "./Wrapper.css";

const usePrevious = (value) => {
	const ref = useRef();
	useEffect(() => {
		ref.current = value;
	});
	return ref.current;
};

export default function App(props) {
	const [previewFile, setPreviewFile] = useState({ file: null, url: "" });
	const { register, errors, handleSubmit } = useForm();
	const {
		gender,
		genderOptions,
		isProcessing,
		userInfo,
		urlImage,
		isUploadAvtSuccess,
		isUpdateProfileSuccess,
		updateUserInfo,
	} = props;
	const prevIsUploadAtvSuccess = usePrevious(isUploadAvtSuccess);
	if (prevIsUploadAtvSuccess !== isUploadAvtSuccess && isUploadAvtSuccess) {
		props.actions.updateUserInfo({
			...updateUserInfo,
			userAvatar: urlImage,
		});
	}
	const onSubmit = (data, e) => {
		let updateUserInfo = {
			...data,
			userGender: gender.value,
		};
		if (data.oldPassword === "" || data.userPassword === "") {
			updateUserInfo = {
				userAvatar: urlImage,
				userGender: gender.value,
			};
		}
		props.actions.uploadAvt({ file: previewFile.file, updateUserInfo });
	};

	const handleUploadFile = () => {
		const input = document.createElement("input");
		input.setAttribute("type", "file");
		input.setAttribute("accept", "image/*");
		input.click();
		input.onchange = () => {
			const file = input.files[0];
			let previewFile = {
				file,
				url: URL.createObjectURL(file),
			};
			setPreviewFile(previewFile);
		};
	};

	return (
		<React.Fragment>
			<form onSubmit={handleSubmit(onSubmit)}>
				<ModalBody>
					{isUpdateProfileSuccess && (
						<Alert color="success">Update profile success</Alert>
					)}
					<FormGroup>
						<div className="d-flex align-items-center justify-content-center">
							<div>
								<figure
									className="avatar mr-3 item-rtl avatar-upload"
									style={{
										position: "relative",
										width: "6.3rem",
										height: "6.3rem",
										border: "1px solid rgba(0,0,0,0.2)",
									}}
									onClick={handleUploadFile}
								>
									<div className="upload-note">Upload</div>
									<img
										src={
											previewFile.file !== null
												? previewFile.url
												: `https://${userInfo.userAvatar}`
										}
										className="rounded-circle"
										alt="profile_avt"
									/>
								</figure>
							</div>
						</div>
					</FormGroup>
					<FormGroup>
						<InputGroup>
							<Input
								type="text"
								name="userName"
								placeholder="Full name"
								innerRef={register({
									required: "This field is required",
									maxLength: {
										value: 50,
										message:
											"Please enter no more than 50 characters",
									},
								})}
								defaultValue={userInfo.userName}
								autoFocus
								invalid={errors.userName ? true : false}
								disabled={isProcessing}
							/>
							<InputGroupAddon addonType="append">
								<InputGroupText>
									<i className="fa fa-user"></i>
								</InputGroupText>
							</InputGroupAddon>
						</InputGroup>
						<FormFeedback>
							{errors.userName && errors.userName.message}
						</FormFeedback>
					</FormGroup>

					<FormGroup>
						<Select
							placeholder="Gender"
							options={genderOptions}
							value={gender}
							onChange={(e) =>
								props.actions.handleSelectChange(e)
							}
                            isSearchable={false}
							isDisabled={isProcessing}
						/>
						<FormFeedback>
							{errors.userGender && errors.userGender.message}
						</FormFeedback>
					</FormGroup>
					<FormGroup>
						<Input
							type="password"
							name="oldPassword"
							placeholder="Password"
							innerRef={register({
								pattern: {
									value: /^(?=.*?[A-Z])(?=.*?[a-z])(?=.*?[0-9])(?=.*?[^\w\s]).{6,}$/,
									message:
										"Password must contain at least 6 characters, 1 numeric character, 1 lowercase letter, 1 uppercase letter and 1 special character",
								},
							})}
							disabled={isProcessing}
							invalid={errors.oldPassword ? true : false}
						/>
						<FormFeedback>
							{errors.oldPassword && errors.oldPassword.message}
						</FormFeedback>
					</FormGroup>
					<FormGroup>
						<Input
							type="password"
							name="userPassword"
							placeholder="Password"
							innerRef={register({
								pattern: {
									value: /^(?=.*?[A-Z])(?=.*?[a-z])(?=.*?[0-9])(?=.*?[^\w\s]).{6,}$/,
									message:
										"Password must contain at least 6 characters, 1 numeric character, 1 lowercase letter, 1 uppercase letter and 1 special character",
								},
							})}
							disabled={isProcessing}
							invalid={errors.userPassword ? true : false}
						/>
						<FormFeedback>
							{errors.userPassword && errors.userPassword.message}
						</FormFeedback>
					</FormGroup>
				</ModalBody>

				<ModalFooter>
					<button type="submit" className="btn btn-primary">
						Save
					</button>
				</ModalFooter>
			</form>
		</React.Fragment>
	);
}
