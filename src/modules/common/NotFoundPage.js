import React, { Component } from "react";
import { Link } from "react-router-dom";
class NotFoundPage extends Component {
	render() {
		return (
			<div className="ps-error bg--cover">
				<div className="bg--overlay"></div>
				<div className="ps-error__content text-center">
					<h1>404</h1>
					<h3>TRANG KHÔNG TÌM THẤY</h3>
					<p>OOPS ! Có vẻ bạn đi ra khỏi ứng dụng ! :(</p>
					<Link className="ps-btn" to="/">
						Quay về trang chủ<i className="fa fa-angle-right"></i>
					</Link>
				</div>
			</div>
		);
	}
}

export default NotFoundPage;
