import React, { Component } from "react";
import { Link } from "react-router-dom";
import { urlFormat } from "services/urlFormat";
import ModalDebate from "./ModalDebate";
import ModalSharing from "./ModalSharing";
import ModalTestEnglish from "./ModalTestEnglish";

class WelcomePage extends Component {
	constructor(props) {
		super(props);
		this.state = {
			isOpen: false,
			isOpenSharing: false,
			isOpenTesting: false,
		};
	}

	render() {
		return (
			<React.Fragment>
				<div className="ps-welcome">
					<div className="ps-welcome--cover">
						<img
							src={urlFormat(
								"/assets/images/background/EC_Website.png"
							)}
							alt="EC_Amazing"
						/>
					</div>
					<div className="welcome-content">
						<div className="link-home">
							<Link to="/home"> English amazing race</Link>
						</div>
						<div className="group-link">
							<ul>
								<li>
									<span
										onClick={() =>
											this.setState({
												isOpenTesting: !this.state
													.isOpenTesting,
											})
										}
									>
										Test your english
									</span>
								</li>
								<li>
									<span
										onClick={() =>
											this.setState({
												isOpen: !this.state.isOpen,
											})
										}
									>
										English debate
									</span>
								</li>
								<li>
									<span
										onClick={() =>
											this.setState({
												isOpenSharing: !this.state
													.isOpenSharing,
											})
										}
									>
										Greetings and sharing
									</span>
								</li>
							</ul>
						</div>
					</div>
				</div>
				<ModalDebate
					isOpen={this.state.isOpen}
					handleToggle={() =>
						this.setState({
							isOpen: false,
						})
					}
				/>
				<ModalSharing
					isOpen={this.state.isOpenSharing}
					handleToggle={() =>
						this.setState({
							isOpenSharing: false,
						})
					}
				/>
				<ModalTestEnglish
					isOpen={this.state.isOpenTesting}
					handleToggle={() =>
						this.setState({
							isOpenTesting: false,
						})
					}
				/>
			</React.Fragment>
		);
	}
}

export default WelcomePage;
