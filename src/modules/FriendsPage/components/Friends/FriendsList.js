import React, { useEffect } from "react";
import FriendsListHeader from "./FriendsListHeader";
import { Col } from "reactstrap";
import styles from "./friendsList.module.css";
import FriendsListItem from "./FriendListItem";
import LoadingScreen from "../../../common/LoadingScreen";
import _ from "lodash";

const FriendsList = (props) => {
	useEffect(() => {
		props.actions.getAllFriends();
	}, [props.actions]);

	const { friendsList, isLoading, message } = props;

	return (
		<div className="chat">
			<FriendsListHeader {...props} />
			<Col lg="12" className={styles.friendGroupContainer}>
				<ul className={styles.friendGroup}>
					{isLoading ? (
						<LoadingScreen />
					) : !_.isEmpty(friendsList) ? (
						friendsList.map((item, index) => {
							return (
								<FriendsListItem
									key={`friend-list-item-${
										item.userFriendId
											? item.userFriendId
											: item.userRequestId
									}`}
									item={item}
									props={props}
								/>
							);
						})
					) : (
						<div className="text-center" style={{ width: "100%" }}>
							{message}
						</div>
					)}
				</ul>
			</Col>
		</div>
	);
};

export default FriendsList;
