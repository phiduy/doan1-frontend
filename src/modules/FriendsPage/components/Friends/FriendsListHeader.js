import React from "react";
import { ReactSVG } from "react-svg";
import FMenu from "../../../common/svg/f-menu.svg";

const FriendsListHeader = (props) => {
	const { userInfo } = props;
	return (
		<div className="chat-header">
			<div className="chat-header-user">
				<figure className="avatar">
					<img
						src={`https://${userInfo.userAvatar}`}
						className="rounded-circle"
						alt="avt"
					/>
				</figure>
				<div>
					<h5>{userInfo.userName}</h5>
				</div>
			</div>
			<div className="chat-header-action">
				<ul className="list-inline">
					<li className="list-inline-item d-xl-none d-inline">
						<span className="btn btn-outline-light mobile-navigation-button">
							<ReactSVG src={FMenu} />
						</span>
					</li>
					<li
						className="list-inline-item"
						data-toggle="tooltip"
						title=""
						data-original-title="All Friends"
						onClick={() => props.actions.getAllFriends()}
					>
						<span className="btn btn-success">All Friends</span>
					</li>
					<li
						className="list-inline-item"
						data-toggle="tooltip"
						title=""
						data-original-title="Friend Requests"
						onClick={() => props.actions.getAllPending()}
					>
						<span className="btn btn-warning text-light">
							Friend Requests
						</span>
					</li>
					<li
						className="list-inline-item"
						data-toggle="tooltip"
						title=""
						data-original-title="Following"
						onClick={() => props.actions.getAllRequests()}
					>
						<span className="btn btn-primary">Following</span>
					</li>
				</ul>
			</div>
		</div>
	);
};

export default FriendsListHeader;
