import React from "react";
import styles from "./friendsList.module.css";

const FriendItemAction = ({ props, item }) => {
	const { curType } = props;
	switch (curType) {
		case 1:
			return (
				<div>
					<span
						className={`${styles.btnCustom} `}
						data-toggle="dropdown"
						aria-expanded="false"
					>
						Friend
					</span>
					<div
						className="dropdown-menu dropdown-menu-right"
						x-placement="bottom-end"
					>
						<span
							className="dropdown-item"
							onClick={() =>
								props.actions.connectRoom({
									sendingId: item.userFriendId,
									...item,
								})
							}
						>
							<i className="fa fa-comment-o text-primary"></i>{" "}
							Chat
						</span>
						<span className="dropdown-item">
							<i className="fa fa-user-times text-danger"></i>{" "}
							Unfriend
						</span>
					</div>
				</div>
			);
		case 2:
			return (
				<div>
					<span
						className={`${styles.btnCustom} `}
						onClick={() =>
							props.actions.postFriendCancelRequest({
								userRequestId: item.userRequestId,
							})
						}
					>
						Cancel Request
					</span>
				</div>
			);
		case 3:
			return (
				<div>
					<span
						className={`${styles.btnCustom} `}
						data-toggle="dropdown"
						aria-expanded="false"
					>
						Status
					</span>
					<div
						className="dropdown-menu dropdown-menu-right"
						x-placement="bottom-end"
					>
						<span
							className="dropdown-item"
							onClick={() =>
								props.actions.postFriendAccept({
									curUserId: item.userFriendId,
									userPendingId: item.userPendingId,
								})
							}
						>
							<i className="fa fa-check text-success"></i> Accept
						</span>
						<span
							className="dropdown-item"
							onClick={() =>
								props.actions.postFriendDeny({
									userPendingId: item.userPendingId,
								})
							}
						>
							<i className="fa fa-user-times text-danger"></i>{" "}
							Delete
						</span>
					</div>
				</div>
			);
		default:
			break;
	}
};

export default FriendItemAction;
