import React, { Component } from "react";
import FriendSideBar from "./SideBar/FriendSideBar";
import FriendsList from "./Friends/FriendsList";

class FriendsPage extends Component {
	render() {
		return (
			<React.Fragment>
				<div className="content">
					<FriendSideBar {...this.props} />
					<FriendsList {...this.props} />
				</div>
			</React.Fragment>
		);
	}
}

export default FriendsPage;
