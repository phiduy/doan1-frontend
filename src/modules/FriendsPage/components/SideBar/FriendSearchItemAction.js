import React from "react";

const FriendSearchItemAction = ({ item, props }) => {
	switch (item.friendStatus) {
		case "friend":
			return (
				<div className="dropdown-menu dropdown-menu-right">
					<span className="dropdown-item">
						<i className="fa fa-user-times text-danger"></i>{" "}
						Unfriend
					</span>
				</div>
			);
		case "normal":
			return (
				<div className="dropdown-menu dropdown-menu-right">
					<span
						className="dropdown-item"
						onClick={() =>
							props.actions.postFriendRequest({
								userRequestId: item._id,
							})
						}
					>
						<i className="fa fa-user-plus text-primary"></i> Add
						Friend
					</span>
					{/* <span className="dropdown-item">
						<i className="fa fa-info text-secondary"></i> Profile
					</span>
					<span className="dropdown-item text-danger">
						<i className="fa fa-ban"></i> Block
					</span> */}
				</div>
			);
		case "request":
			return (
				<div className="dropdown-menu dropdown-menu-right">
					<span className="dropdown-item">
						<i className="fa fa-ban text-primary"></i> Cancel
						request
					</span>
					<span className="dropdown-item">
						<i className="fa fa-user-times text-danger"></i> Profile
					</span>
				</div>
			);
		case "pending":
			return (
				<div className="dropdown-menu dropdown-menu-right">
					<span
						className="dropdown-item"
						onClick={() =>
							props.actions.postFriendAccept({
								userPendingId: item._id,
							})
						}
					>
						<i className="fa fa-check text-primary"></i> Accept
					</span>
					<span className="dropdown-item">
						<i className="fa fa-ban text-primary"></i> Cancel
						request
					</span>
					<span className="dropdown-item">
						<i className="fa fa-user-times text-danger"></i> Profile
					</span>
				</div>
			);
		default:
			break;
	}
};

export default FriendSearchItemAction;
