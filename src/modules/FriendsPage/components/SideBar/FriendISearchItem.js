import React from "react";
import { ReactSVG } from "react-svg";
import FHorizontal from "../../../common/svg/f-horizontal.svg";
import FriendSearchItemAction from "./FriendSearchItemAction";
import styles from "./sidebar.module.css";

const FriendItem = ({ item, props }) => {
	return (
		<li className="list-group-item">
			<div>
				<figure className="avatar">
					<img
						src={`https://${item.userAvatar}`}
						className="rounded-circle"
						alt="avatar"
					/>
				</figure>
			</div>
			<div className="users-list-body">
				<div className={styles.userInfo}>
					<div className={styles.userName}>{item.userName}</div>
				</div>
				<div className="users-list-action">
					<div className="dropdown">
						<span data-toggle="dropdown">
							<ReactSVG src={FHorizontal} />
						</span>
						<FriendSearchItemAction item={item} props={props} />
					</div>
				</div>
			</div>
		</li>
	);
};

export default FriendItem;
