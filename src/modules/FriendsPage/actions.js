/**
 * @file All actions will be listed here
 */

import { createAction } from 'redux-actions';
import * as CONST from './constants';


export const connectRoom = createAction(CONST.CONNECT_ROOM);
export const connectRoomFail = createAction(CONST.CONNECT_ROOM_FAIL);

export const getAllFriends = createAction(CONST.GET_ALL_FRIENDS);
export const getAllFriendsSuccess = createAction(CONST.GET_ALL_FRIENDS_SUCCESS);
export const getAllFriendsFail = createAction(CONST.GET_ALL_FRIENDS_FAIL);

export const getAllPending = createAction(CONST.GET_ALL_PENDING);
export const getAllPendingSuccess = createAction(CONST.GET_ALL_PENDING_SUCCESS);
export const getAllPendingFail = createAction(CONST.GET_ALL_PENDING_FAIL);

export const getAllRequests = createAction(CONST.GET_ALL_REQUESTS);
export const getAllRequestsSuccess = createAction(CONST.GET_ALL_REQUESTS_SUCCESS);
export const getAllRequestsFail = createAction(CONST.GET_ALL_REQUESTS_FAIL);

export const postFriendRequest = createAction(CONST.POST_FRIEND_REQUEST);
export const postFriendRequestSuccess = createAction(CONST.POST_FRIEND_REQUEST_SUCCESS);
export const postFriendRequestFail = createAction(CONST.POST_FRIEND_REQUEST_FAIL);

export const postFriendAccept = createAction(CONST.POST_FRIEND_ACCEPT);
export const postFriendAcceptSuccess = createAction(CONST.POST_FRIEND_ACCEPT_SUCCESS);
export const postFriendAcceptFail = createAction(CONST.POST_FRIEND_ACCEPT_FAIL);

export const postFriendDeny = createAction(CONST.POST_FRIEND_DENY);
export const postFriendDenySuccess = createAction(CONST.POST_FRIEND_DENY_SUCCESS);
export const postFriendDenyFail = createAction(CONST.POST_FRIEND_DENY_FAIL);

export const postFriendCancelRequest = createAction(CONST.POST_FRIEND_CANCEL_REQUEST);
export const postFriendCancelRequestSuccess = createAction(CONST.POST_FRIEND_CANCEL_REQUEST_SUCCESS);
export const postFriendCancelRequestFail = createAction(CONST.POST_FRIEND_CANCEL_REQUEST_FAIL);

export const searchUser = createAction(CONST.SEARCH_USER);
export const searchUserSuccess = createAction(CONST.SEARCH_USER_SUCCESS);
export const searchUserFail = createAction(CONST.SEARCH_USER_FAIL);
/*---------------------------------------------------------------------------------------------------------------------------------*/
export const toggleLoading = createAction(CONST.TOGGLE_LOADING);
export const getUserInfo = createAction(CONST.GET_USER_INFO);