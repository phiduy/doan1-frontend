import { call, put } from "redux-saga/effects";
import { push } from "react-router-redux";
import { delay } from "redux-saga";
import * as actions from "../actions";
import * as messageActions from "../../MessagesPage/actions";
import * as friendAPI from "api/friend";
import { takeAction } from "services/forkActionSagas";
import { socket } from "services/api";
import { get } from "services/localStoredService";
import MESSAGES from "../../../constants/messages";

// Handle Connect Room
export function* handleConnectRoom(action) {
	try {
		socket.emit(MESSAGES.CLIENT_JOIN_ROOM, {
			userId: get("userId"),
			...action.payload,
		});
		yield put(messageActions.getConnectRoom(action.payload));
		yield put(push("/messages"));
	} catch (error) {
		yield put(actions.connectRoomFail(error));
	}
}

// Handle Get All Friends
export function* handleGetAllFriends(action) {
	try {
		let res = yield call(friendAPI.getAllFriends);
		yield put(actions.toggleLoading({ type: "friendList", value: true }));
		yield delay(500);
		switch (res.status) {
			case 200:
				yield put(actions.getAllFriendsSuccess(res.data.data));
				yield put(
					actions.toggleLoading({ type: "friendList", value: false })
				);
				break;
			case 406:
				yield put(
					actions.getAllFriendsFail("You don't have any friends")
				);
				yield put(
					actions.toggleLoading({ type: "friendList", value: false })
				);
				break;
			default:
				break;
		}
	} catch (err) {
		yield put(actions.getAllFriendsFail(err));
	}
}

// Handle Get All Request
export function* handleGetAllRequests(action) {
	try {
		let res = yield call(friendAPI.getAllRequests);
		yield put(actions.toggleLoading({ type: "friendList", value: true }));
		yield delay(500);
		switch (res.status) {
			case 200:
				yield put(actions.getAllRequestsSuccess(res.data.data));
				yield put(
					actions.toggleLoading({ type: "friendList", value: false })
				);
				break;
			case 406:
				yield put(
					actions.getAllRequestsFail(
						"You don't have any friend requests"
					)
				);
				yield put(
					actions.toggleLoading({ type: "friendList", value: false })
				);
				break;
			default:
				break;
		}
	} catch (err) {
		yield put(actions.getAllRequestsFail(err));
	}
}

// Handle Get All Pending
export function* handleGetAllPending(action) {
	try {
		let res = yield call(friendAPI.getAllPending);
		yield put(actions.toggleLoading({ type: "friendList", value: true }));
		yield delay(500);
		switch (res.status) {
			case 200:
				yield put(actions.getAllPendingSuccess(res.data.data));
				yield put(
					actions.toggleLoading({ type: "friendList", value: false })
				);
				break;
			case 406:
				yield put(
					actions.getAllPendingFail("You don't have anyone following")
				);
				yield put(
					actions.toggleLoading({ type: "friendList", value: false })
				);
				break;
			default:
				break;
		}
	} catch (err) {
		yield put(actions.getAllPendingFail(err));
	}
}

// Handle Post Friend Request
export function* handlePostFriendRequest(action) {
	try {
		socket.emit(MESSAGES.CLIENT_SEND_FRIEND_REQUEST, {
			userId: get("userId"),
			sendingId: action.payload.userRequestId,
		});
		let res = yield call(friendAPI.postFriendRequest, action.payload);
		yield put(actions.toggleLoading({ type: "friendList", value: true }));
		yield delay(500);
		switch (res.status) {
			case 200:
				yield put(actions.searchUser({ userName: "", page: 0 }));
				yield put(actions.postFriendRequestSuccess(res.data.data));
				yield put(
					actions.toggleLoading({ type: "friendList", value: false })
				);
				break;
			case 406:
				yield put(actions.postFriendRequestFail());
				yield put(
					actions.toggleLoading({ type: "friendList", value: false })
				);
				break;
			default:
				break;
		}
	} catch (err) {
		yield put(actions.postFriendRequestFail(err));
	}
}

// Handle Post Friend Request
export function* handlePostFriendAccept(action) {
	try {
		let res = yield call(friendAPI.postFriendAccept, action.payload);
		yield delay(500);
		switch (res.status) {
			case 200:
				yield put(actions.getAllFriends());
				yield put(actions.postFriendAcceptSuccess(res.data.data));
				break;
			case 406:
				yield put(actions.postFriendAcceptFail());
				break;
			default:
				break;
		}
	} catch (err) {
		yield put(actions.postFriendAcceptFail(err));
	}
}

// Handle Post Friend Deny
export function* handlePostFriendDeny(action) {
	try {
		let res = yield call(friendAPI.postFriendDeny, action.payload);
		yield delay(500);
		switch (res.status) {
			case 200:
				yield put(actions.getAllFriends());
				yield put(actions.postFriendDenySuccess(res.data.data));
				break;
			case 406:
				yield put(actions.postFriendDenyFail());
				break;
			default:
				break;
		}
	} catch (err) {
		yield put(actions.postFriendDenyFail(err));
	}
}

// Handle Post Friend Deny
export function* handlePostFriendCancelRequest(action) {
	try {
		let res = yield call(friendAPI.postFriendCancelRequest, action.payload);
		yield delay(500);
		switch (res.status) {
			case 200:
				yield put(actions.postFriendCancelRequestSuccess(res.data.data));
				break;
			case 406:
				yield put(actions.postFriendCancelRequestFail());
				break;
			default:
				break;
		}
	} catch (err) {
		yield put(actions.postFriendCancelRequestFail(err));
	}
}


// Handle Search User
export function* handleSearchUser(action) {
	try {
		let res = yield call(friendAPI.searchUser, action.payload);
		yield delay(500);
		if (res.status === 200) {
			yield put(actions.searchUserSuccess(res.data.data));
		} else {
			yield put(actions.searchUserFail());
		}
	} catch (err) {
		yield put(actions.searchUserFail(err));
	}
}

/*----------------------------------------------------------*/
export function* getAllFriends() {
	yield takeAction(actions.getAllFriends, handleGetAllFriends);
}
export function* getAllRequests() {
	yield takeAction(actions.getAllRequests, handleGetAllRequests);
}
export function* getAllPending() {
	yield takeAction(actions.getAllPending, handleGetAllPending);
}
export function* postFriendRequest() {
	yield takeAction(actions.postFriendRequest, handlePostFriendRequest);
}
export function* postFriendAccept() {
	yield takeAction(actions.postFriendAccept, handlePostFriendAccept);
}
export function* postFriendDeny() {
	yield takeAction(actions.postFriendDeny, handlePostFriendDeny);
}
export function* postFriendCancelRequest(){
    yield takeAction(actions.postFriendCancelRequest, handlePostFriendCancelRequest);
}
export function* searchUser() {
	yield takeAction(actions.searchUser, handleSearchUser);
}
export function* connectRoom() {
	yield takeAction(actions.connectRoom, handleConnectRoom);
}
/*----------------------------------------------------------*/
export default [
	getAllFriends,
	getAllPending,
	getAllRequests,
    postFriendRequest,
    postFriendCancelRequest,
    postFriendAccept,
    postFriendDeny,
	searchUser,
	connectRoom,
];
